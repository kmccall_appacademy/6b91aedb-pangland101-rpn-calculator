class RPNCalculator
  # TODO: your code goes here!


  def initialize
    @sym_convert = { "+" => :+, "-" => :-, "*" => :*, "/" => :/ }
    @clone = [] #less sure about this one, but we'll play it by ear
    @math_stack = []
  end

  def push(int)
    @math_stack.push(int)
  end

  def plus
    operation(:+)
  end

  def minus
    operation(:-)
  end

  def times
    operation(:*)
  end

  def divide
    operation(:/)
  end

  def value
    @math_stack[-1]
  end

  def tokens(postfix_string)
    tokenized = []

    postfix_string.split(" ").each do |el|
      if @sym_convert[el]
        tokenized.push(@sym_convert[el])
      else
        tokenized.push(el.to_i)
      end
    end

    tokenized
  end

  def evaluate(str)
    @clone = tokens(str)
    @clone.each do |el|
      el.is_a?(Integer) ? @math_stack.push(el) : operation(el)
    end

    @clone = @math_stack

    value
  end

  private

  def operation(el)
    raise "calculator is empty" if @math_stack.size < 2

    case el
    when :+
      @math_stack.push(@math_stack.pop + @math_stack.pop)
    when :-
      @math_stack.push(-@math_stack.pop + @math_stack.pop)
    when :*
      @math_stack.push(@math_stack.pop * @math_stack.pop)
    when :/
      @math_stack.push(@math_stack.pop.to_f**-1 * @math_stack.pop)
    else
      raise "Illegal operator was used. Try again, cheater."
    end
  end
end
